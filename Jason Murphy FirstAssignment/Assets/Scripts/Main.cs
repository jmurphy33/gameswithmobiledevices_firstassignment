﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
    public GameObject effect;

    public GameObject selectedObject;

    [SerializeField]

    // object movement
    float dragSpeed = 0.1f, _horizontallimit = 100, _verticallimit = 100;
    Vector3 startingPos, endPos;

    // object scaling
    float initialFingersDistance;
    Vector3 initialScale;

    // object rotation
    float rotationRate = 1;
    Vector2 scrollPos;
    bool wasRotating = false;

    // cam zoom 
    float perspectiveZoomSpeed = 0.5f;
    float orthoZoomSpeed = 0.5f;

    
    // cam rotation
    private Vector3 point1;
    private Vector3 point2;
    private float xAngle = 0.0f;
    private float yAngle = 0.0f;
    private float zAngle = 0.0f;
    private float xAnglePlaceholder = 0.0f;
    private float yAnglePlaceholder = 0.0f;
    private float zAnglePlaceholder = 0.0f;

    // camera movement
    float v = 0.0f;
    float h = 0.0f;

    void Start()
    {
        xAngle = 0.0f;
        yAngle = 0.0f;
        zAngle = 0.0f;
        Camera.main.transform.rotation = Quaternion.Euler(yAngle, xAngle, 0.0f);
    }

    // Update is called once per frame
    void Update()
    {
        // camera functionality for moving and rotating
        if (GameObject.FindGameObjectWithTag("Movement").GetComponent<Toggle>().isOn)
        {     
            // apply camera decisions       
            cameraDecisions();
        }
        // extra functionality for spawning effects and zooming
        else if (GameObject.FindGameObjectWithTag("Extra").GetComponent<Toggle>().isOn)
        {     
            // apply extra decision   
            extraDecisions();
        }
        // if neither option is selected the object options take effect
        else
        {
            // select an object or leave current object selected
            select();

            // if an object has been selected
            if (selectedObject != null)
            {
                // apply object decisions
                objectDecisions();
            }
        }

    }

    /// <summary>
    /// Determines which object in the scene is selected
    /// Only objects with the tag move can be selected
    /// </summary>
    void select()
    {
        if (Input.touchCount == 1)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                Ray r = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit hit;

                if (Physics.Raycast(r.origin, r.direction, out hit, 1000))
                {
                    if (hit.collider.gameObject.tag == "move")
                    {
                        changeColor(Color.red);
                        selectedObject = hit.collider.gameObject;
                        changeColor(Color.blue);

                    }

                }

            }
        }
    }

    /// <summary>
    /// Changes colour of selected object
    /// </summary>
    /// <param name="c"></param>
    void changeColor(Color c)
    {
        // if an object has been selected already
        if (selectedObject != null)
        {
            // turn selected object a certain colour
            selectedObject.GetComponent<Renderer>().material.color = c;
        }
    }

    /// <summary>
    /// Determines how many touches where taken in
    /// </summary>
    void objectDecisions()
    {
        // object movement relative to camera
        if (Input.touchCount == 3)
        {
            ObjectMove();
        }

        // scaling
        else if (Input.touchCount == 2)
        {
            ObjectScale();
        }

        // rotation 
        else if (Input.touchCount == 1)
        {
            ObjectRotation();

        }
    }

    /// <summary>
    /// Extra options for zoom functionality and spawning objects
    /// </summary>
    void extraDecisions()
    {
        // zooming in an out
        if (Input.touchCount == 2)
        {
            CamZoom();
        }

        // spawn objects in three locations
        if(Input.touchCount == 3)
        {
            Spawn();
        }     
    }
   
    /// <summary>
    /// Camera options for moving and rotating the camera
    /// </summary>
    void cameraDecisions()
    {
        // move left/right/up/down
        if (Input.touchCount == 3)
        {
            CamMove();
        }

        // move forward/backwards
        else if (Input.touchCount == 2)
        {
            CamForward();
        }

        // rotate
        else if (Input.touchCount == 1)
        {
            CamRotation();
        }
    }

    // Extra options

    /// <summary>
    /// Extra: Spawns objects where three fingers are pressed
    /// These objects are deleted when fingers are released
    /// </summary>
    void Spawn()
    {
        // get the forward plane
        Plane fPlane = new Plane(Camera.main.transform.forward, Vector3.zero);
        
        // creates three rays for each touch on the screen
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        Ray ray1 = Camera.main.ScreenPointToRay(Input.GetTouch(1).position);
        Ray ray2 = Camera.main.ScreenPointToRay(Input.GetTouch(2).position);

        // create three objects to spawn
        GameObject g1 = null, g2= null, g3=null;

        // apply spawn to each touch
        SpawnImpl(Input.GetTouch(0),fPlane,ray,g1);
        SpawnImpl(Input.GetTouch(1),fPlane, ray1, g2);
        SpawnImpl(Input.GetTouch(2),fPlane, ray2, g2);
        

    }

    /// <summary>
    /// Implementation of the spawn method
    /// </summary>
    /// <param name="fPlane"></param>
    /// <param name="ray"></param>
    /// <param name="g"></param>
    void SpawnImpl(Touch t, Plane fPlane, Ray ray, GameObject g)
    {
        // switch for touch 0, 1, 2
        switch (t.phase)
        {
            case TouchPhase.Moved:

                float distance = 0f;
                // cast ray from forward plane 
                if (fPlane.Raycast(ray, out distance))
                {
                    // instantiate object using effect game object, the point returned from the ray and default rotation
                    g = (GameObject)Instantiate(effect, ray.GetPoint(distance), Quaternion.identity);
                    // destroy after 1 second
                    Destroy(g, 1);
                }

                break;

            case TouchPhase.Ended:
                if (g)
                {
                    // if touch has ended before 1 second destroy object
                    Destroy(g);
                }            
                break;
        }
    }

    /// <summary>
    /// Extra: Zoom functionality
    /// </summary>
    void CamZoom()
    {
        // get first touch
        Touch t0 = Input.GetTouch(0);
        // get second touch
        Touch t1 = Input.GetTouch(1);
        // get prevous first touch
        Vector2 previousT0 = t0.position - t0.deltaPosition;
        // get previous second touch
        Vector2 previousT1 = t1.position - t1.deltaPosition;

        // get previous magnitude using previous first and second touch
        float previousMagnitude = (previousT0 - previousT1).magnitude;
        // get current magnitude using current first and second touch
        float currentMagnitude = (t0.position - t1.position).magnitude;
        // get the difference between the two magnitudes
        float magnitudeDifference = previousMagnitude - currentMagnitude;

        // apply zoom to orthograhpc camera
        if (Camera.main.orthographic)
        {
            // alter zoom using magnitude and speed
            Camera.main.orthographicSize += magnitudeDifference * orthoZoomSpeed;
            Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize, 0.1f);
        }
        // apply zoom to perspective based camera
        else
        {
            // alter zoom using magnitude and speed
            Camera.main.fieldOfView += magnitudeDifference * perspectiveZoomSpeed;
            Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView, 0.1f, 179.9f);
        }
    }


    // Object options

    /// <summary>
    /// Object Decision: Scale by moving two fingers closer 
    /// or further apart.
    /// </summary>
    void ObjectScale()
    {

        switch (Input.GetTouch(0).phase)
        {
            case TouchPhase.Began:
                // get the previous distance between first and second touch
                initialFingersDistance = Vector2.Distance(Input.touches[0].position, Input.touches[1].position);
                // set the scale to the local scale of the selected object
                initialScale = selectedObject.transform.localScale;
                break;

            case TouchPhase.Moved:
                // get the current distance between first and second touch
                var currentFingerDistance = Vector2.Distance(Input.touches[0].position, Input.touches[1].position);
              
                // to avoid getting NaN exception check the distance is greater than 0
                if (initialFingersDistance != 0)
                {
                    // set the amout the scale be to the the current distance divided by the previous distance
                    var scaleFactor = currentFingerDistance / initialFingersDistance;
                    // scale the object
                    selectedObject.transform.localScale = initialScale * scaleFactor;
                }
                break;
        }
    }

    /// <summary>
    /// Object Decision: Rotate the object using one finger
    /// </summary>
    void ObjectRotation()
    {
        switch (Input.GetTouch(0).phase)
        {
            case TouchPhase.Began:
                // get the first touch position
                point1 = Input.GetTouch(0).position;
                // set placeholders for x, y and z 
                xAnglePlaceholder = xAngle;
                yAnglePlaceholder = yAngle;
                zAnglePlaceholder = zAngle;
                break;

            case TouchPhase.Moved:
                // get the second touch position
                point2 = Input.GetTouch(0).position;

                // using previous placeholders get new x, y and z by +/- their axis'
                // and multiplying by the screen width or height
                xAngle = xAnglePlaceholder + (point2.x - point1.x) * 180.0f / Screen.width;
                yAngle = yAnglePlaceholder - (point2.y - point1.y) * 90.0f / Screen.height;
                zAngle = zAnglePlaceholder + (point2.z = point1.z);

                // rotate the selected object to the new rotation
                selectedObject.transform.rotation = Quaternion.Euler(yAngle, xAngle, zAngle);
                break;
        }
    }

    /// <summary>
    /// Object Decision: Move object using three finger touch
    /// </summary>
    void ObjectMove()
    {
        // get the horizontal plane
        Plane hPlane = new Plane(Camera.main.transform.forward, Vector3.zero);
        
        // get ray using first touch
        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        switch (Input.GetTouch(0).phase)
        {
            case TouchPhase.Moved:

                float distance = 0f;
                if (hPlane.Raycast(ray, out distance))
                {
                    // move object to where the ray returns a point using the distance
                    selectedObject.transform.position = ray.GetPoint(distance);
                }

                break;
                
        }
    }


    // Camera Options

    /// <summary>
    /// Camera Decision: Move camera forward or backwards on the Z axis with 
    /// two finger touch
    /// </summary>
    void CamForward()
    {
        var touch = Input.GetTouch(0);

        if (touch.phase == TouchPhase.Moved)
        {
            v = 1 * touch.deltaPosition.y;
            Camera.main.transform.position += Camera.main.transform.forward * v * Time.deltaTime;
        }
    }

    /// <summary>
    /// Camera Decision: Move camera left/right/up/down using three finger touch
    /// </summary>
    void CamMove()
    {
        switch (Input.GetTouch(0).phase)
        {
            case TouchPhase.Moved:
                // get delta position of touch 0 by a speed and delta time
                Vector2 touchDeltaPos = Input.GetTouch(0).deltaPosition * 1 * Time.deltaTime;
                // move camera on the x and y using the delta postion
                Camera.main.transform.Translate(touchDeltaPos.x, touchDeltaPos.y, 0);
                break;
        }
    }

    /// <summary>
    /// Camera Rotation: Rotates using 1 finger touch
    /// </summary>
    void CamRotation()
    {
        switch (Input.GetTouch(0).phase)
        {
            case TouchPhase.Began:
                // get point 1 for touch 0 position
                point1 = Input.GetTouch(0).position;
                // get placeholders for x and y axis
                xAnglePlaceholder = xAngle;
                yAnglePlaceholder = yAngle;
                break;

            case TouchPhase.Moved:
                // get point 2 for touch 1 position
                point2 = Input.GetTouch(0).position;

                // determine x and y using the placeholders of the previous positions 
                // +/- the x and ys of point 1 and 2 multiplided by the screen height/width
                xAngle = xAnglePlaceholder + (point2.x - point1.x) * 180.0f / Screen.width;
                yAngle = yAnglePlaceholder - (point2.y - point1.y) * 90.0f / Screen.height;

                // rotate using new x and y found and 0 for z
                Camera.main.transform.rotation = Quaternion.Euler(yAngle, xAngle, 0.0f);
                break;
        }
    }

}
